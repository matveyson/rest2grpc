//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_RESPONSE_HPP
#define REST2GRPC_RESPONSE_HPP

#include <string>
#include <cstdio>
#include <utility>
#include <map>

namespace HTTP {
    class Response {
    public:
        uint16_t status = 200;
        std::string statusMessage = "OK";
        std::map<std::string, std::string> headers;
        std::string data;

        std::string ToString() {
            std::string result;
            AddHeader("Content-Length", std::to_string(data.length()));
            result += ("HTTP/1.1 " + std::to_string(status) + " " + statusMessage + "\r\n");
            result += HeadersToText();
            result += "\r\n";
            return result;
        }

        void AddHeader(const std::string& key, std::string value) {
            headers[key] = std::move(value);
        }

    private:
        std::string HeadersToText() {
            std::string result;
            for (const auto &header: headers)
                result += (header.first + ": " + header.second + "\r\n");
            return result;
        }
    };
}
#endif //REST2GRPC_RESPONSE_HPP
