//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_WORKER_HPP
#define REST2GRPC_WORKER_HPP

#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <cstdlib>
#include <ev++.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <cerrno>
#include <list>
#include <string>
#include <iostream>

#include "buffer.hpp"
#include "request.hpp"
#include "response.hpp"

namespace Connection {
    class Server;

    class Worker {
    public:
        Worker(int s, Connection::Server &server);

        virtual ~Worker();

    private:
        ev::io io;
        Connection::Server &server;
        int sfd;

        // Buffers that are pending write
        std::list<Buffer *> write_queue;
        std::unique_ptr<HTTP::Request> request = std::make_unique<HTTP::Request>();
        std::unique_ptr<HTTP::Response> response = std::make_unique<HTTP::Response>();

        // Generic callback
        void handler(ev::io &watcher, int revents);
        // Socket is writable
        void write_cb(ev::io &watcher);
        // Receive message from client socket
        void read_cb(ev::io &watcher);
    };
}
#endif //REST2GRPC_WORKER_HPP
