//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_REQUEST_HPP
#define REST2GRPC_REQUEST_HPP

#include <string>
#include <iostream>
#include "../third_party/http-parser/http_parser.h"

namespace HTTP {
    enum httpMethod {
        UNDEF, GET, DELETE, POST, PUT, PATCH,
    };

    class Request {
    public:
        std::string tcp_body;
        bool has_header;
        bool has_body;
        httpMethod http_method;
        uint32_t content_length;
        std::string url;

        void AddPacket(const std::string& data) {
            tcp_body += data;

            http_method = ParseMethod();
            has_header = FindHeader();
            content_length = GetContentLength();
            url = GetURL();

            size_t headEnd = tcp_body.find("\r\n\r\n") + 4;
            printf("body: length %lu, header 0-%zu, cl %d, url %s\n", tcp_body.length(), headEnd, content_length, url.c_str());
        }

        bool IsCompleted() {
            size_t head_end = tcp_body.find("\r\n\r\n") + 4;
            if(ShouldHaveBody())
                return has_header && (tcp_body.length() == head_end + content_length);
            return has_header;
        }

    private:
        bool ShouldHaveBody() {
            return http_method > 2; // POST, PUT, PATCH
        }

        bool FindHeader() {
            size_t headEnd = tcp_body.find("\r\n\r\n") + 4;
            return headEnd - 4 != std::string::npos;
        }

        std::string GetURL() {
            size_t methodEnd = tcp_body.find(' ') + 1;
            size_t urlEnd = tcp_body.find(' ', methodEnd);
            return tcp_body.substr(methodEnd, urlEnd - methodEnd);
        }

        uint32_t GetContentLength() {
            size_t CL_start = tcp_body.find("Content-Length: ");
            if (has_header and CL_start != std::string::npos)
                return std::stoll(tcp_body.substr(CL_start + 16, tcp_body.find('\r', CL_start) - CL_start - 16));
            return 0;
        }

        httpMethod ParseMethod() {
            size_t methodEnd = tcp_body.find(' ');
            if (methodEnd != std::string::npos) {
                std::string method_parsed = tcp_body.substr(0, methodEnd);
                if (method_parsed == "GET") return GET;
                else if (method_parsed == "POST") return POST;
                else if (method_parsed == "PUT") return PUT;
                else if (method_parsed == "PATCH") return PATCH;
                else if (method_parsed == "DELETE") return DELETE;
            }
            return UNDEF;
        }
    };
}
#endif //REST2GRPC_REQUEST_HPP
