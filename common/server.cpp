//
// Created by Matvey Levinson on 21.12.2019.
//

#include "server.hpp"

void Connection::Server::io_accept(ev::io &watcher, int revents) {
    if (ev::ERROR & revents) return perror("Event invalid");

    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);
    int client_sd = accept(watcher.fd, (struct sockaddr *) &client_addr, &client_len);
    printf("accept > %s:%d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

    if (client_sd < 0) return perror("Accept error");

    std::unique_ptr<Worker> worker_instance(new Worker(client_sd, *this));
    workers.insert(std::pair<int, std::unique_ptr<Worker>>(client_sd, std::move(worker_instance)));
}

void Connection::Server::signal_cb(ev::sig &signal, int revents) {
    signal.loop.break_loop();
    printf("Recieved %d, closing\n", signal.signum);
}

Connection::Server::Server(int port) {
    printf("Listening > 127.0.0.1:%d\n", port);

    struct sockaddr_in addr;
    s = socket(PF_INET, SOCK_STREAM, 0);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    int opt = 1;
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) == -1) perror("setsockopt");
    if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) != 0) perror("bind");

    // Enable non-blocking mode
    fcntl(s, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK);
    listen(s, 5);

    // Starting Socket handler
    io.set<Server, &Server::io_accept>(this);
    io.start(s, ev::READ);

    // Starting SIGINT/SIGTERM handler
    sio_int.set<&Server::signal_cb>();
    sio_int.start(SIGINT);
    sio_term.set<&Server::signal_cb>();
    sio_term.start(SIGTERM);
}

Connection::Server::~Server() {
    printf("Destructing server");
    shutdown(s, SHUT_RDWR);
    close(s);
}

void Connection::Server::FreeWorker(int client_sd) {
    close(client_sd);
    workers.erase(client_sd);
}
