//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_SERVER_HPP
#define REST2GRPC_SERVER_HPP

#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <cstdlib>
#include <ev++.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <cerrno>
#include <list>
#include <string>
#include <iostream>
#include <map>

#include "worker.hpp"

namespace Connection {
    class Server {
    private:
        ev::io io;
        ev::sig sio_int;
        ev::sig sio_term;
        std::map<int, std::unique_ptr<Worker>> workers;
        int s;

    public:
        void FreeWorker(int client_sd);

        void io_accept(ev::io &watcher, int revents);

        static void signal_cb(ev::sig &signal, int revents);

        explicit Server(int port);

        virtual ~Server();
    };
}
#endif //REST2GRPC_SERVER_HPP
