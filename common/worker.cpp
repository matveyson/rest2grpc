//
// Created by Matvey Levinson on 21.12.2019.
//

#include "server.hpp"

Connection::Worker::Worker(int s, Connection::Server &server) : sfd(s), server(server) {
    fcntl(s, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK);

    io.set<Worker, &Worker::handler>(this);
    io.start(s, ev::READ);
}

Connection::Worker::~Worker() {
    io.stop();
}

void Connection::Worker::handler(ev::io &watcher, int revents) {
    if (ev::ERROR & revents)
        return perror("got invalid event");

    if (revents & ev::READ)
        read_cb(watcher);

    if (revents & ev::WRITE)
        write_cb(watcher);

    if (write_queue.empty()) {
        io.set(ev::READ);
    } else {
        io.set(ev::READ | ev::WRITE);
    }
}

void Connection::Worker::write_cb(ev::io &watcher) {
    printf("           write_cb\n");
    if (write_queue.empty()) {
        io.set(ev::READ);
        return;
    }

    Buffer *buffer = write_queue.front();

    ssize_t written = write(watcher.fd, buffer->dpos(), buffer->nbytes());
    if (written < 0)
        return perror("write error");

    buffer->pos += written;
    if (buffer->nbytes() == 0) {
        write_queue.pop_front();
        delete buffer;
    }
}

void Connection::Worker::read_cb(ev::io &watcher) {

    printf("           read_cb\n");
    char buffer[BUFSIZ];

    ssize_t nread = recv(watcher.fd, buffer, sizeof(buffer), 0);

    if (nread < 0)
        return perror("read error");

    if (nread == 0) {
        server.FreeWorker(sfd);
        return;
    }
    request->AddPacket(buffer);
    if (!request->IsCompleted()) return;
    request = std::make_unique<HTTP::Request>();
//            NoteClient notes(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials()));
//            NoteClient notes;
//            std::string reply = notes.List();

    write_queue.push_back(new Buffer(response->ToString().c_str(), response->ToString().length()));
}
