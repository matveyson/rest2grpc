//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_GRPC_СPP
#define REST2GRPC_GRPC_СPP

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>
#include "../protos_compiled/notes.grpc.pb.h"
#include "../protos_compiled/notes.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

class NoteClient {
public:
//    NoteClient(std::shared_ptr<Channel> channel) : stub_(NoteService::NewStub(channel)) {}
//    NoteClient(): stub_(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials())){}
    NoteClient(){
        this->stub_ = NoteService::NewStub(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials()));
    }

    std::string List() {
        Empty request;
        NoteList reply;
        ClientContext context;
        Status status = stub_->List(&context, request, &reply);

        if (status.ok()) {
            return "ok";
        } else {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return "RPC failed";
        }
    }

private:
    std::unique_ptr<NoteService::Stub> stub_;
};

//int main() {
//    //grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials())
//    NoteClient notes;
//    std::string reply = notes.List();
//    std::cout << "Greeter received: " << reply << std::endl;
//
//    return 0;
//}

#endif //REST2GRPC_GRPC_СPP
