//
// Created by Matvey Levinson on 16.12.2019.
//

#ifndef REST2GRPC_BUFFER_HPP
#define REST2GRPC_BUFFER_HPP
namespace Connection{
    struct Buffer {
        char *data;
        ssize_t len;
        ssize_t pos;

        Buffer(const char *bytes, ssize_t nbytes) {
            pos = 0;
            len = nbytes;
            data = new char[nbytes];
            memcpy(data, bytes, nbytes);
        }

        virtual ~Buffer() {
            delete[] data;
        }

        char *dpos() {
            return data + pos;
        }

        ssize_t nbytes() {
            return len - pos;
        }
    };
}
#endif //REST2GRPC_BUFFER_HPP
