FROM ubuntu:lts
WORKDIR /app

RUN apt-get update -y && apt-get install -y libev-dev

COPY . /app
RUN