#include <ev++.h>
#include "common/server.hpp"

#define PORT 1337

int main(int argc, char **argv) {
    int port = PORT;

    if (argc > 1) port = atoi(argv[1]);

    ev::default_loop loop;
    Connection::Server app(port);

    loop.run(0);

    return 0;
}