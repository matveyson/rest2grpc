compile:
	protoc -I=./protos/ --cpp_out=./protos_compiled ./protos/notes.proto
	protoc -I=./protos/ --grpc_out=./protos_compiled --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ./protos/notes.proto